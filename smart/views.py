# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views import generic, View
from django.http import HttpResponseRedirect

from django.shortcuts import render, redirect
from .models import TrashModel, TaskModel
from .forms import TrashForm, TaskForm

from rms.helper import get_size
from rms.cleaner import delete

import os


class StartView(generic.TemplateView):
    template_name = "start.html"

    def get_context_data(self, **kwargs):
        context = super(StartView, self).get_context_data(**kwargs)
        return context


class FileManager(generic.TemplateView):
    template_name = "file_manager.html"
    default_path = os.path.expanduser("~")

    def get_current_path(self):
        path = self.default_path

        if self.request.GET.get("path"):
            path = self.request.GET["path"]
        return path

    @staticmethod
    def get_listdir(path):
        list_dir = [{"name": os.pardir,
                     "abspath": os.path.split(os.path.abspath(path))[0],
                     "type": "pardir"}]
        for f in os.listdir(path):
            abspath = os.path.join(path, f)
            if not os.access(abspath, os.R_OK):
                if os.path.isfile(abspath):
                    list_dir.append({"name": f,
                                     "abspath": abspath,
                                     "type": "no access file"})
                elif os.path.isdir(abspath):
                    list_dir.append({"name": f,
                                     "abspath": abspath,
                                     "type": "no access dir"})
            elif os.path.isfile(abspath):
                list_dir.append({"name": f,
                                 "abspath": abspath,
                                 "type": "file"})
            elif os.path.isdir(abspath):
                list_dir.append({"name": f,
                                 "abspath": abspath,
                                 "type": "dir"})
            elif os.path.islink(abspath):
                list_dir.append({"name": f,
                                 "abspath": abspath,
                                 "type": "no access dir"})
        list_dir.sort()
        return list_dir

    @staticmethod
    def get_pardirs(path):
        dirs = []
        current_dir = os.path.abspath(path)
        dir_name = os.path.basename(path)
        while len(dir_name):
            dirs.append({"name": dir_name, "abspath": current_dir})
            current_dir = os.path.dirname(current_dir)
            dir_name = os.path.basename(current_dir)
        else:
            dirs.append({"name": os.path.sep, "abspath": os.path.sep})
        dirs.reverse()
        return dirs

    def get_context_data(self, **kwargs):
        context = super(FileManager, self).get_context_data(**kwargs)

        path = self.get_current_path()
        list_dir = self.get_listdir(path)
        dirs = self.get_pardirs(path)

        context["dirs"] = dirs
        context["list_dir"] = list_dir
        context["task_form"] = TaskForm
        return context

    def post(self, request, *args, **kwargs):
        if "remove" in request.POST and "selected-file" in request.POST:
            t = TrashModel.objects.filter(id=request.POST["trash"]).first()

            if "dryrun" in request.POST:
                dryrun = True
            else:
                dryrun = False

            if not request.POST["regex"]:
                action = "remove"
            else:
                action = "regex"

            task = TaskModel.objects.create(action=action,
                                            regex=request.POST["regex"],
                                            status="inprogress",
                                            trash=t,
                                            dryrun=dryrun)
            task.set_files(request.POST.getlist("selected-file"))
            task.save()

            try:
                task.run()
            except:
                pass
        return render(
            request=request,
            context=self.get_context_data(),
            template_name=self.template_name,
        )

class TrashList(generic.TemplateView):
    template_name = "trash_list.html"

    def get_context_data(self, **kwargs):
        context = super(TrashList, self).get_context_data(**kwargs)
        context["trashes"] = TrashModel.objects.all()
        return context


class TrashCreate(View):
    template_name = "trash_create.html"
    form_class = TrashForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {"form": form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            trash_model = form.save(commit=False)
            trash_model.trash_path = os.path.expanduser(trash_model.trash_path)
            trash_model.save()
            return HttpResponseRedirect("../trash-list")
        return render(request, self.template_name, {"form": form})


class TrashDetail(generic.DetailView):
    template_name = "trash_detail.html"
    model = TrashModel

    def get_context_data(self, **kwargs):
        context = super(TrashDetail, self).get_context_data(**kwargs)
        tr = super(TrashDetail, self).get_object()
        path = os.path.join(tr.trash_path, tr.name, "trash", "files")
        context["trash"] = tr
        context["current_size"] = get_size(path)

        if os.path.exists(path):
            file_list = FileManager.get_listdir(path)
            file_list.pop(0)
            context["files"] = file_list
        else:
            context["files"] = []
        return context

    def post(self, request, *args, **kwargs):
        t = super(TrashDetail, self).get_object()

        if "delete" in request.POST:
            delete(os.path.join(t.trash_path, t.name))
            t.delete()
            return redirect("trash_list")
        else:
            if "dryrun" in request.POST:
                dryrun = request.POST["dryrun"]
            else:
                dryrun = False

            if "trash delete" in request.POST:
                action = "trash delete"
            elif "restore" in request.POST:
                action = "restore"
            elif "clean" in request.POST:
                action = "clean"
            else:
                return redirect(".")

            task = TaskModel.objects.create(action=action,
                                            trash=t,
                                            status="inprogress",
                                            dryrun=dryrun)
            task.set_files(request.POST.getlist("selected-file"))
            task.save()
            try:
                task.run()
            except:
                pass
            return redirect(".")


class TaskList(generic.TemplateView):
    template_name = "tasks_list.html"

    def get_context_data(self, **kwargs):
        context = super(TaskList, self).get_context_data(**kwargs)
        context["tasks"] = TaskModel.objects.all().reverse()
        return context

    def post(self, request, *args, **kwargs):
        tasks = TaskModel.objects.all()
        tasks.delete()
        return HttpResponseRedirect("/tasks-list")
