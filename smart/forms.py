from django import forms
from .models import TrashModel, TaskModel


class TrashForm(forms.ModelForm):
    class Meta:
        model = TrashModel
        fields = ("name", "trash_path", "trash_size", "clean_policy", "remove_policy", "restore_policy")


class TaskForm(forms.ModelForm):
    class Meta:
        model = TaskModel
        fields = ("trash", "regex", "dryrun")
