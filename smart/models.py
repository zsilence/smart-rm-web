# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from rms.trash import Trash

from functools import partial

import pathos.multiprocessing as multiprocessing

import os
import json


class TrashModel(models.Model):
    re_policy_list = (
        ("collision", "collision"),
        ("replace", "replace")
    )

    clean_policy_list = (
        ("date", "date"),
        ("size", "size")
    )

    name = models.CharField("trash name", max_length=30, default="trash", unique=True)
    trash_path = models.CharField("trash path", max_length=255, default="~")
    trash_size = models.IntegerField("trash size", default=1024*1024*1024)
    clean_policy = models.CharField("clean policy", max_length=30, choices=clean_policy_list, default="date")
    remove_policy = models.CharField("remove policy", max_length=30, choices=re_policy_list, default="replace")
    restore_policy = models.CharField("restore policy", max_length=30, choices=re_policy_list, default="replace")

    def __str__(self):
        return self.name

    def get_trash(self, dryrun):
        t = Trash(dryrun=dryrun,
                  silent=True,
                  interactive=False,
                  force=True,
                  trash_size=self.trash_size,
                  remove_policy=self.remove_policy,
                  clean_policy=self.clean_policy,
                  restore_policy=self.restore_policy,
                  trash_path=os.path.expanduser(os.path.join(self.trash_path, self.name))
                  )
        return t


class TaskModel(models.Model):
    actions = (
        ("remove", "remove"),
        ("restore", "restore"),
        ("regex", "regex"),
        ("trash delete", "trash delete"),
        ("clean", "clean")
    )

    statuses = (
        ("complete", "complete"),
        ("inprogress", "inprogress"),
        ("error", "error"),
    )

    status = models.CharField(max_length=30, choices=statuses)
    action = models.CharField(max_length=30, choices=actions)
    files = models.TextField()
    trash = models.ForeignKey(TrashModel)
    regex = models.CharField(max_length=50, blank=True, null=True)
    dryrun = models.BooleanField(verbose_name="dry-run", default=False)
    error_descr = models.CharField(max_length=500, blank=True, null=True)

    def run(self):
        try:
            pool_size = multiprocessing.cpu_count() * 2
            pool = multiprocessing.Pool(processes=pool_size)

            files = self.get_files()
            tr = self.trash.get_trash(self.dryrun)

            if self.action == "remove":
                pool.map(tr.remove, files)
            elif self.action == "regex":
                regex_remove = partial(tr.regex_remove, regex=self.regex)
                pool.map(regex_remove, files)
            elif self.action == "restore":
                names = [os.path.split(f)[1] for f in files]
                pool.map(tr.restore, names)
            elif self.action == "clean":
                tr.clean_trash()
            elif self.action == "trash delete":
                names = [os.path.split(f)[1] for f in files]
                pool.map(tr.delete_from_trash, names)

            pool.close()
            pool.join()
            self.status = "complete"
            self.save()
        except Exception as e:
            self.status = "error"
            self.error_descr = e
            self.save()

    def set_files(self, files):
        self.files = json.dumps(files)

    def get_files(self):
        return json.loads(self.files)
