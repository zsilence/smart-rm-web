from django.conf.urls import url
from . import views
from .models import TrashModel


urlpatterns = [
    url(r"^$", views.StartView.as_view(), name="startview"),
    url(r"^manager/(?P<path>.+)?", views.FileManager.as_view(), name="view_files"),
    url(r"^trash-list/$", views.TrashList.as_view(), name="trash_list"),
    url(r"^trash-create/$", views.TrashCreate.as_view(), name="trash_create"),
    url(r"^trash-detail/(?P<pk>[0-9]+)/$", views.TrashDetail.as_view(), name="trash_detail"),
    url(r"^tasks-list/$", views.TaskList.as_view(), name="tasks_list"),
]
